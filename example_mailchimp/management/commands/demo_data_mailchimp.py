# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from gdpr.models import Consent
from mailchimp.models import MailChimpSettings


class Command(BaseCommand):

    help = "MailChimp Demo Data"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        consent = Consent.objects.init_consent(
            "offers",
            "Special Offers",
            "Would you like to receive special offers..?",
            False,
        )
        mailchimp_settings = MailChimpSettings.load()
        mailchimp_settings.default_consent = consent
        mailchimp_settings.save()
        self.stdout.write("Complete: {}".format(self.help))
