# -*- encoding: utf-8 -*-
import logging

from celery import task
from mailchimp.models import Campaign


logger = logging.getLogger(__name__)


@task()
def mailchimp_sync():
    logger.info("Sync MailChimp")
    count = Campaign.objects.sync()
    logger.info("Sync MailChimp - {} records - complete".format(count))
    return count
