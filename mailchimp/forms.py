# -*- encoding: utf-8 -*-
from django import forms

from .models import MailChimpSettings


class MailChimpSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["default_consent"]
        f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = MailChimpSettings
        fields = ("default_consent",)
