# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.db.models import F
from django.urls import reverse
from django.views.generic import ListView, UpdateView

from base.view_utils import BaseMixin
from .forms import MailChimpSettingsForm
from .models import Campaign, MailChimpSettings, MailingList


class CampaignListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def get_queryset(self):
        return Campaign.objects.all().order_by(
            F("updated").desc(nulls_last=True)
        )


class MailingListListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def get_queryset(self):
        return MailingList.objects.all().order_by(
            F("updated").desc(nulls_last=True)
        )


class MailChimpSettingsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = MailChimpSettingsForm
    model = MailChimpSettings

    def get_object(self):
        return self.model.load()

    def get_success_url(self):
        return reverse("project.settings")
