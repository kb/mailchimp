# -*- encoding: utf-8 -*-
from django.conf.urls import url

from .views import (
    CampaignListView,
    MailChimpSettingsUpdateView,
    MailingListListView,
)


urlpatterns = [
    url(
        regex=r"^campaign/$",
        view=CampaignListView.as_view(),
        name="mailchimp.campaign.list",
    ),
    url(
        regex=r"^list/$",
        view=MailingListListView.as_view(),
        name="mailchimp.list.list",
    ),
    url(
        regex=r"^settings/update/$",
        view=MailChimpSettingsUpdateView.as_view(),
        name="mailchimp.settings.update",
    ),
]
