# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from mailchimp.models import Campaign


class Command(BaseCommand):

    help = "Sync MailChimp"

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        result = Campaign.objects.sync()
        # for k, v in result.items():
        #     self.stdout.write("  {}   {}".format(k, v))
        self.stdout.write(
            "{} - {} records - complete...".format(self.help, len(result))
        )
