# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.factories import UserFactory
from login.tests.fixture import perm_check
from .factories import CampaignFactory, MailingListFactory


@pytest.mark.django_db
def test_campaign_list(perm_check):
    CampaignFactory()
    url = reverse("mailchimp.campaign.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_mailchimp_settings_update(perm_check):
    url = reverse("mailchimp.settings.update")
    perm_check.staff(url)


@pytest.mark.django_db
def test_mailing_list_list(perm_check):
    MailingListFactory()
    url = reverse("mailchimp.list.list")
    perm_check.staff(url)
