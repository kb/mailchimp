# -*- encoding: utf-8 -*-
"""
Test for the ``MailingList`` model and manager.

.. note:: Non-contact related tests are in the ``mailchimp/tests`` folder.

"""
import pytest
import pytz
import responses

from datetime import datetime
from http import HTTPStatus

from contact.tests.factories import ContactEmailFactory, ContactFactory
from gdpr.models import UserConsent
from gdpr.tests.factories import ConsentFactory
from mailchimp.models import MailChimpClient
from mailchimp.tests.factories import MailingListFactory


def _member_json():
    return {
        "members": [
            {
                "email_address": "patrick@kbsoftware.co.uk",
                "status": "unsubscribed",
                "unsubscribe_reason": "N/A (Unknown)",
                "last_changed": "2015-11-29T20:13:05+00:00",
            },
            {
                "email_address": "code@pkimber.net",
                "status": "subscribed",
                "last_changed": "2015-11-29T20:14:54+00:00",
            },
        ]
    }


def _member_json_empty():
    return {"members": []}


def _member_url():
    return "https://us12.api.mailchimp.com/3.0/lists/03a58242f8/members"


@pytest.mark.django_db
def test_str():
    """Return the campaigns where the 'subscriber_clicks' has changed."""
    mailing_list = MailingListFactory(list_id="abc", name="Apple")
    assert "Apple ('abc')" == str(mailing_list)


@pytest.mark.django_db
@responses.activate
def test_sync_members():
    consent = ConsentFactory()
    contact_1 = ContactFactory()
    ContactEmailFactory(contact=contact_1, email="patrick@kbsoftware.co.uk")
    contact_2 = ContactFactory()
    ContactEmailFactory(contact=contact_2, email="code@pkimber.net")
    responses.add(
        responses.GET, _member_url(), json=_member_json(), status=HTTPStatus.OK
    )
    responses.add(
        responses.GET,
        _member_url(),
        json=_member_json_empty(),
        status=HTTPStatus.OK,
    )
    mailing_list = MailingListFactory(list_id="03a58242f8")
    client = MailChimpClient()
    mailing_list.sync_members(client, consent)
    # check consent records were written
    assert 1 == UserConsent.objects.for_user(contact_1.user).count()
    assert 1 == UserConsent.objects.for_user(contact_2.user).count()
    contact_1_consent = UserConsent.objects.for_user(contact_1.user).first()
    contact_2_consent = UserConsent.objects.for_user(contact_2.user).first()
    assert "N/A (Unknown)" == contact_1_consent.comment
    assert "" == contact_2_consent.comment
    assert (
        datetime(2015, 11, 29, 20, 13, 5, tzinfo=pytz.utc)
        == contact_1_consent.date_updated
    )
    assert (
        datetime(2015, 11, 29, 20, 14, 54, tzinfo=pytz.utc)
        == contact_2_consent.date_updated
    )
    # check consent records were updated
    assert UserConsent.objects.consent_given(contact_1.user, consent) is False
    assert UserConsent.objects.consent_given(contact_2.user, consent) is True


@pytest.mark.django_db
@responses.activate
def test_sync_members_updated_since():
    """User update of their consent since the MailChimp interaction."""
    consent = ConsentFactory()
    contact = ContactFactory()
    ContactEmailFactory(contact=contact, email="patrick@kbsoftware.co.uk")
    responses.add(
        responses.GET, _member_url(), json=_member_json(), status=HTTPStatus.OK
    )
    responses.add(
        responses.GET,
        _member_url(),
        json=_member_json_empty(),
        status=HTTPStatus.OK,
    )
    mailing_list = MailingListFactory(list_id="03a58242f8")
    client = MailChimpClient()
    mailing_list.sync_members(client, consent)
    # check consent records were written
    assert 1 == UserConsent.objects.count()
    assert 1 == UserConsent.objects.for_user(contact.user).count()
    user_consent = UserConsent.objects.for_user(contact.user).first()
    assert "N/A (Unknown)" == user_consent.comment
    assert (
        datetime(2015, 11, 29, 20, 13, 5, tzinfo=pytz.utc)
        == user_consent.date_updated
    )
    # check consent records were updated
    assert UserConsent.objects.consent_given(contact.user, consent) is False
