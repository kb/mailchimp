# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from gdpr.tests.factories import ConsentFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from mailchimp.models import MailChimpSettings


@pytest.mark.django_db
def test_mailchimp_settings(client):
    mailchimp_settings = MailChimpSettings.load()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    consent = ConsentFactory()
    response = client.post(
        reverse("mailchimp.settings.update"), {"default_consent": consent.pk}
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("project.settings") == response.url
    mailchimp_settings = MailChimpSettings.load()
    assert consent.slug == mailchimp_settings.default_consent.slug
