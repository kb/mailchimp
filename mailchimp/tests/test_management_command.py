# -*- encoding: utf-8 -*-
import pytest
import responses

from django.core.management import call_command
from http import HTTPStatus

from gdpr.tests.factories import ConsentFactory
from mailchimp.tests.factories import MailChimpSettingsFactory


def _list_json():
    return {"total_items": 1, "lists": []}


def _member_json():
    return {"members": []}


@pytest.mark.django_db
def test_init_app():
    call_command("init_app_mailchimp")


@pytest.mark.django_db
@responses.activate
def test_mailchimp_sync():
    MailChimpSettingsFactory(default_consent=ConsentFactory())
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/campaigns",
        json={
            "campaigns": [
                {
                    "id": "24702d304a",
                    "create_time": "2016-10-31T12:47:49+00:00",
                    "status": "sent",
                    "send_time": "2016-10-31T12:51:02+00:00",
                    "recipients": {"list_id": "3341b14801"},
                    "settings": {"title": "Apple Sales"},
                    "report_summary": {"subscriber_clicks": 26},
                }
            ],
            "total_items": 1,
        },
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/lists",
        json=_list_json(),
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/lists/3341b14801/members",
        json=_member_json(),
        status=HTTPStatus.OK,
    )
    call_command("mailchimp_sync")
