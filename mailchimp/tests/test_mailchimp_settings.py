# -*- encoding: utf-8 -*-
import pytest

from mailchimp.models import MailChimpSettings, MailChimpSettingsError
from mailchimp.tests.factories import MailChimpSettingsFactory
from gdpr.tests.factories import ConsentFactory


@pytest.mark.django_db
def test_get_default_consent():
    consent = ConsentFactory(slug="apple", name="Apple")
    MailChimpSettingsFactory(default_consent=consent)
    mailchimp_settings = MailChimpSettings.load()
    assert consent == mailchimp_settings.get_default_consent()


@pytest.mark.django_db
def test_get_default_consent_no_consent():
    mailchimp_settings = MailChimpSettings.load()
    with pytest.raises(MailChimpSettingsError) as e:
        mailchimp_settings.get_default_consent()
    assert "MailChimp settings are not set-up" in str(e.value)


@pytest.mark.django_db
def test_str():
    consent = ConsentFactory(slug="apple", name="Apple")
    MailChimpSettingsFactory(default_consent=consent)
    mailchimp_settings = MailChimpSettings.load()
    assert "MailChimp Settings: Default Consent: Apple ('apple')" == str(
        mailchimp_settings
    )


@pytest.mark.django_db
def test_str_no_consent():
    mailchimp_settings = MailChimpSettings.load()
    assert "MailChimp Settings" == str(mailchimp_settings)
