# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from mailchimp.models import NotFoundError, MemberData


def test_consent_given():
    data = MemberData(
        status="subscribed", comment="apple", updated=timezone.now()
    )
    assert data.consent_given() is True
    assert "apple" == data.get_comment()


def test_consent_given_cleaned():
    data = MemberData(status="cleaned", comment="apple", updated=timezone.now())
    assert data.consent_given() is False
    assert (
        "Cleaned (hard bounce or repeatedly soft bounced) (apple)"
        == data.get_comment()
    )


def test_consent_given_cleaned_empty_comment():
    data = MemberData(status="cleaned", comment="", updated=timezone.now())
    assert data.consent_given() is False
    assert (
        "Cleaned (hard bounce or repeatedly soft bounced)" == data.get_comment()
    )


def test_consent_given_cleaned_no_comment():
    data = MemberData(status="cleaned", comment=None, updated=timezone.now())
    assert data.consent_given() is False
    assert (
        "Cleaned (hard bounce or repeatedly soft bounced)" == data.get_comment()
    )


def test_consent_given_not():
    data = MemberData(
        status="unsubscribed", comment="apple", updated=timezone.now()
    )
    assert data.consent_given() is False
    assert "apple" == data.get_comment()


def test_consent_given_unknown():
    data = MemberData(
        status="does-not-exist", comment="apple", updated=timezone.now()
    )
    with pytest.raises(NotFoundError) as e:
        data.consent_given()
    assert "Unrecognised status: 'does-not-exist'" in str(e.value)
