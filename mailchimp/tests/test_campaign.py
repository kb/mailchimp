# -*- encoding: utf-8 -*-
import pytest
import pytz
import responses

from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from http import HTTPStatus

from gdpr.tests.factories import ConsentFactory
from mailchimp.models import Campaign, MailingList
from mailchimp.tests.factories import (
    CampaignFactory,
    MailChimpSettingsFactory,
    MailingListFactory,
)


def _campaign_json():
    return {
        "campaigns": [
            {
                "id": "24702d304a",
                "create_time": "2016-10-31T12:47:49+00:00",
                "status": "sent",
                "send_time": "2016-10-31T12:51:02+00:00",
                "recipients": {"list_id": "3341b14801"},
                "settings": {"title": "Apple Sales"},
                "report_summary": {"subscriber_clicks": 26},
            }
        ],
        "total_items": 1,
    }


def _list_json():
    return {"total_items": 1, "lists": []}


def _member_json():
    return {"members": []}


@pytest.mark.django_db
def test_create_campaign():
    created = datetime(2018, 9, 24, 16, 0, 0, tzinfo=pytz.utc)
    mailing_list = MailingListFactory(list_id="orange")
    campaign = Campaign.objects.create_campaign(
        "abc", "Apple", "sent", created, mailing_list, None
    )
    campaign.refresh_from_db()
    assert "abc" == campaign.campaign_id
    assert "Apple" == campaign.title
    assert "sent" == campaign.status
    assert created == campaign.created
    assert "orange" == campaign.mailing_list.list_id
    assert campaign.send_time is None
    assert campaign.subscriber_clicks is None


@pytest.mark.django_db
def test_is_old_campaign():
    campaign = CampaignFactory(
        send_time=timezone.now() - relativedelta(months=+3)
    )
    assert campaign.is_old_campaign() is True


@pytest.mark.django_db
def test_is_old_campaign_not_sent():
    campaign = CampaignFactory(send_time=None)
    assert campaign.is_old_campaign() is False


@pytest.mark.django_db
def test_is_old_campaign_recent():
    campaign = CampaignFactory(send_time=timezone.now())
    assert campaign.is_old_campaign() is False


@pytest.mark.django_db
def test_mailing_lists():
    mailing_list_1 = MailingListFactory(list_id="a")
    mailing_list_2 = MailingListFactory(list_id="b")
    CampaignFactory(campaign_id="c1", mailing_list=mailing_list_1)
    CampaignFactory(campaign_id="c2", mailing_list=mailing_list_2)
    CampaignFactory(campaign_id="c3", mailing_list=mailing_list_1)
    assert set(["a", "b"]) == Campaign.objects._mailing_list_list_ids(
        {"c1": 1, "c2": 2, "c3": 3}
    )


@pytest.mark.django_db
def test_str():
    """Return the campaigns where the 'subscriber_clicks' has changed."""
    campaign = CampaignFactory(
        campaign_id="abc", title="Apple", subscriber_clicks=12
    )
    assert "Apple ('abc')" == str(campaign)


@pytest.mark.django_db
@responses.activate
def test_sync():
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/campaigns",
        json=_campaign_json(),
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/lists",
        json=_list_json(),
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/lists/3341b14801/members",
        json=_member_json(),
        status=HTTPStatus.OK,
    )
    MailChimpSettingsFactory(default_consent=ConsentFactory())
    assert {"24702d304a": 26} == Campaign.objects.sync()
    assert 1 == Campaign.objects.count()
    campaign = Campaign.objects.first()
    assert "24702d304a" == campaign.campaign_id
    assert "Apple Sales" == campaign.title
    assert "sent" == campaign.status
    assert (
        datetime(2016, 10, 31, 12, 47, 49, tzinfo=pytz.utc) == campaign.created
    )
    assert "3341b14801" == campaign.mailing_list.list_id
    assert (
        datetime(2016, 10, 31, 12, 51, 2, tzinfo=pytz.utc) == campaign.send_time
    )
    assert campaign.subscriber_clicks is None
    assert timezone.now().date() == campaign.updated.date()
    assert 1 == MailingList.objects.count()
    mailing_list = MailingList.objects.first()
    assert "3341b14801" == mailing_list.list_id
    assert "" == mailing_list.name
    assert timezone.now().date() == mailing_list.updated.date()


@pytest.mark.django_db
@responses.activate
def test_sync_subscriber_clicks_has_changed():
    """Return the campaigns where the 'subscriber_clicks' has changed."""
    CampaignFactory(campaign_id="24702d304a", subscriber_clicks=12)
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/campaigns",
        json=_campaign_json(),
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/lists",
        json=_list_json(),
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://us12.api.mailchimp.com/3.0/lists/3341b14801/members",
        json=_member_json(),
        status=HTTPStatus.OK,
    )
    MailChimpSettingsFactory(default_consent=ConsentFactory())
    assert {"24702d304a": 26} == Campaign.objects.sync()
