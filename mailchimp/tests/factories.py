# -*- encoding: utf-8 -*-
import factory

from django.utils import timezone

from gdpr.tests.factories import ConsentFactory
from mailchimp.models import Campaign, MailChimpSettings, MailingList


class MailChimpSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MailChimpSettings

    default_consent = factory.SubFactory(ConsentFactory)


class MailingListFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MailingList

    @factory.sequence
    def list_id(n):
        return "list_id_{}".format(n + 1)


class CampaignFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Campaign

    created = timezone.now()
    mailing_list = factory.SubFactory(MailingListFactory)

    @factory.sequence
    def campaign_id(n):
        return "campaign_id_{}".format(n + 1)
