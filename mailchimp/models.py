# -*- encoding: utf-8 -*-
import attr

from dateutil.parser import parse
from django.apps import apps
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone
from http import HTTPStatus
from mailchimp3 import MailChimp
from mailchimp3.mailchimpclient import MailChimpError
from reversion import revisions as reversion

from base.singleton import SingletonModel
from gdpr.models import Consent, UserConsent


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


class MailChimpSettingsError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class NotFoundError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


@attr.s
class MemberData:
    status = attr.ib()
    comment = attr.ib()
    updated = attr.ib()

    def _is_cleaned(self):
        """Has the contact been *cleaned*?

        Cleaned contacts have addresses that have hard bounced, or repeatedly
        soft bounced, and are considered invalid:
        https://mailchimp.com/help/about-cleaned-contacts/

        """
        return self.status == "cleaned"

    def consent_given(self):
        """Check the MailChimp status to see if consent is given."""
        result = None
        if self._is_cleaned():
            result = False
        elif self.status == "subscribed":
            result = True
        elif self.status == "unsubscribed":
            result = False
        else:
            raise NotFoundError("Unrecognised status: '{}'".format(self.status))
        return result

    def get_comment(self):
        if self._is_cleaned():
            result = "Cleaned (hard bounce or repeatedly soft bounced)"
            if self.comment:
                result = "{} ({})".format(result, self.comment)
        else:
            result = self.comment
        return result


class MailChimpClient:

    PAGE_SIZE = 100

    def __init__(self):
        self.client = MailChimp(
            mc_api=settings.MAILCHIMP_API_KEY,
            mc_user=settings.MAILCHIMP_USER_NAME,
        )

    def _member_data(self, row):
        last_changed = row["last_changed"]
        updated = parse(last_changed)
        comment = row.get("unsubscribe_reason")
        status = row["status"]
        return MemberData(status=status, comment=comment, updated=updated)

    def campaigns(self):
        data = self.client.campaigns.all(
            get_all=True,
            fields=(
                "campaigns.id,campaigns.create_time,campaigns.status,"
                "campaigns.settings.title,campaigns.send_time,"
                "campaigns.send_time,campaigns.recipients.list_id,"
                "campaigns.report_summary.subscriber_clicks"
            ),
        )
        return data["campaigns"]

    def mailing_lists(self):
        result = {}
        data = self.client.lists.all(get_all=True, fields="lists.name,lists.id")
        for row in data["lists"]:
            result[row["id"]] = row["name"]
        return result

    def members(self, list_id, page_number):
        """Find the members of a list (by page number)

        28/09/2018 I checked the MailChimp UI and found Campaigns with missing
        Lists.  I phoned the client to check, and they have deleted Lists.

        To handle this, we could allow our users to mark a list as deleted in
        the UI.  This isn't too much help though, because they will forget and
        this app will then throw errors.

        Perhaps we check the send date of the campaign and if it is more than
        one month old, ignore the missing list?

        """
        result = {}
        try:
            offset = page_number * self.PAGE_SIZE
            data = self.client.lists.members.all(
                list_id,
                count=self.PAGE_SIZE,
                offset=offset,
                fields=(
                    "members.email_address,"
                    "members.status,"
                    "members.unsubscribe_reason,"
                    "members.last_changed,"
                ),
            )
            members = data["members"]
            for row in members:
                result[row["email_address"]] = self._member_data(row)
        except MailChimpError as e:
            if e.args and e.args[0].get("status") == HTTPStatus.NOT_FOUND:
                raise NotFoundError(e)
            else:
                raise
        return result


class MailChimpSettings(SingletonModel):
    default_consent = models.ForeignKey(Consent, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "MailChimp Settings"
        verbose_name_plural = "MailChimp Settings"

    def __str__(self):
        result = "MailChimp Settings"
        try:
            result = "{}: Default Consent: {} ('{}')".format(
                result, self.default_consent.name, self.default_consent.slug
            )
        except ObjectDoesNotExist:
            pass
        return result

    def get_default_consent(self):
        try:
            return self.default_consent
        except ObjectDoesNotExist:
            raise MailChimpSettingsError("MailChimp settings are not set-up")


reversion.register(MailChimpSettings)


class MailingListManager(models.Manager):
    def create_mailing_list(self, list_id, name=None):
        obj = self.model(list_id=list_id)
        if name:
            obj.name = name
        obj.save()
        return obj

    def init_mailing_list(self, list_id, name=None):
        try:
            obj = self.model.objects.get(list_id=list_id)
            if name:
                obj.name = name
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_mailing_list(list_id, name)
        return obj

    def sync(self, client):
        data = client.mailing_lists()
        for list_id, name in data.items():
            self.init_mailing_list(list_id, name)


class MailingList(models.Model):
    list_id = models.SlugField(unique=True)
    name = models.CharField(max_length=100, blank=True)
    updated = models.DateTimeField(blank=True, null=True)
    objects = MailingListManager()

    class Meta:
        ordering = ["name", "list_id"]
        verbose_name = "Mailing List"
        verbose_name_plural = "Mailing Lists"

    def __str__(self):
        return "{} ('{}')".format(self.name, self.list_id)

    def _update_contacts(self, email, member_data, default_consent):
        """Update the consent record for the contact.

        .. note:: We might find more than one contact with the same email
                  address.

        Keyword arguments:
        email -- email address of the contact
        member_data -- Subscribe/unsubscribe information (``MemberData``)

        """
        contacts = get_contact_model().objects.matching_email(email)
        for contact in contacts:
            consent_exists = UserConsent.objects.consent_exists_for_user(
                default_consent,
                contact.user,
                member_data.consent_given(),
                member_data.updated,
                member_data.get_comment(),
            )
            if not consent_exists:
                UserConsent.objects.set_consent(
                    default_consent,
                    member_data.consent_given(),
                    user=contact.user,
                    comment=member_data.get_comment(),
                    date_updated=member_data.updated,
                )

    def sync_members(self, client, default_consent):
        page_number = 0
        while True:
            result = client.members(self.list_id, page_number)
            for email, member_data in result.items():
                self._update_contacts(email, member_data, default_consent)
            if result:
                page_number = page_number + 1
            else:
                break
        self.updated = timezone.now()
        self.save()


class CampaignManager(models.Manager):
    def _mailing_list_list_ids(self, campaign_clicks):
        """Return the list ID for the campaign mailing lists."""
        return {
            x.mailing_list.list_id
            for x in self.model.objects.filter(
                campaign_id__in=campaign_clicks.keys()
            )
        }

    def _sync_campaign(self, client):
        campaign_clicks = {}
        campaigns = client.campaigns()
        for row in campaigns:
            subscriber_clicks = None
            campaign_id = row["id"]
            title = row["settings"]["title"]
            created = parse(row["create_time"])
            status = row["status"]
            send_time = row["send_time"]
            send_time = parse(send_time) if send_time else None
            list_id = row["recipients"]["list_id"]
            mailing_list = MailingList.objects.init_mailing_list(list_id)
            report_summary = row.get("report_summary")
            if report_summary:
                subscriber_clicks = report_summary["subscriber_clicks"]
            campaign = self.init_campaign(
                campaign_id=campaign_id,
                title=title,
                status=status,
                created=created,
                mailing_list=mailing_list,
                send_time=send_time,
            )
            if subscriber_clicks == campaign.subscriber_clicks:
                pass
            else:
                campaign_clicks[campaign_id] = subscriber_clicks
        return campaign_clicks

    def create_campaign(
        self, campaign_id, title, status, created, mailing_list, send_time
    ):
        obj = self.model(
            campaign_id=campaign_id,
            title=title,
            status=status,
            created=created,
            mailing_list=mailing_list,
            send_time=send_time,
        )
        obj.save()
        return obj

    def init_campaign(
        self, campaign_id, title, status, created, mailing_list, send_time
    ):
        try:
            obj = self.model.objects.get(campaign_id=campaign_id)
            obj.title = title
            obj.status = status
            obj.created = created
            obj.mailing_list = mailing_list
            obj.send_time = send_time
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_campaign(
                campaign_id, title, status, created, mailing_list, send_time
            )
        return obj

    def sync(self):
        """Synchronise campaigns and return the ones which need checking.

        So how do I manage this?

        The ``_sync_campaign`` method returns a dictionary containing the
        campaign ID and the number of ``subscriber_clicks`` for updated
        campaigns e.g::

          {"24702d304a": 26, "37460e6d0c": 52}

        To Do

        1. Iterate through the campaigns.
        2. Get the mailing lists for the campaigns.
        3. Synchronise the mailing lists.
        3.1. After each mailing list, update the campaigns which use the list.

        So do I need to keep a record of when the list (or campaign) was last
        updated?

        - It would be good to do this... if only for the information of the
          user when looking in *Settings*.

        """
        client = MailChimpClient()
        mailchimp_settings = MailChimpSettings.load()
        campaign_clicks = self._sync_campaign(client)
        # update the names of the mailing lists
        MailingList.objects.sync(client)
        for campaign_id, subscriber_clicks in campaign_clicks.items():
            campaign = self.model.objects.get(campaign_id=campaign_id)
            campaign.sync_members(
                client, mailchimp_settings.get_default_consent()
            )
        return campaign_clicks


class Campaign(models.Model):
    campaign_id = models.SlugField(unique=True)
    title = models.CharField(max_length=100)
    created = models.DateTimeField()
    status = models.CharField(max_length=50)
    mailing_list = models.ForeignKey(MailingList, on_delete=models.CASCADE)
    send_time = models.DateTimeField(blank=True, null=True)
    subscriber_clicks = models.IntegerField(blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)
    objects = CampaignManager()

    class Meta:
        ordering = ["-created"]
        verbose_name = "Campaign"
        verbose_name_plural = "Campaigns"

    def __str__(self):
        return "{} ('{}')".format(self.title, self.campaign_id)

    def is_old_campaign(self):
        """The campaign is *old* if it was sent more than 90 days ago."""
        result = False
        if self.send_time:
            days = (timezone.now() - self.send_time).days
            if days > 90:
                result = True
        return result

    def sync_members(self, client, default_consent):
        """Synchronise the members (if the campaign has been sent)."""
        try:
            # has the campaign been sent yet?
            if self.send_time:
                self.mailing_list.sync_members(client, default_consent)
                self.updated = timezone.now()
                self.save()
        except NotFoundError:
            if self.is_old_campaign():
                # an old campaign, so don't worry if the list has been deleted
                pass
            else:
                raise
