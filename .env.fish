source venv-mailchimp/bin/activate.fish
set -x KUBECONFIG (k3d get-kubeconfig)

set -x DATABASE_HOST (kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
set -x DATABASE_NAME "dev_app_mailchimp_$USER"
set -x DATABASE_PASS "postgres"
set -x DATABASE_PORT (kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services kb-dev-db-postgresql)
set -x DATABASE_USER "postgres"
set -x DEFAULT_FROM_EMAIL "web@pkimber.net"
set -x DJANGO_SETTINGS_MODULE "example_mailchimp.dev_$USER"
set -x MAIL_TEMPLATE_TYPE "django"
set -x MAILCHIMP_API_KEY "11111e111ee1e1e1111111111eeee11e-us12"
set -x MAILCHIMP_USER_NAME "my-mailchimp-user-name"
set -x SECRET_KEY "the_secret_key"
source .private

echo "KUBECONFIG:" $KUBECONFIG
echo "DATABASE_HOST:" $DATABASE_HOST
echo "DATABASE_NAME:" $DATABASE_NAME
echo "DATABASE_PORT:" $DATABASE_PORT
echo "DJANGO_SETTINGS_MODULE:" $DJANGO_SETTINGS_MODULE
